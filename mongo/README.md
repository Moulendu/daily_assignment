# Ansible Role for MongoDB

### MongoDB
MongoDB is a NoSQL database which stores the data in form of key-value pairs. It is an Open Source, Document Database which provides high performance and scalability along with data modelling and data management of huge sets of data in an enterprise application.

### About Role
This Role will Install Mongo on remote server according to User Requirements, Which means if user will decide whether he wants a replication of mongo or not, Which version user wants to install.

## Requirements
* Python should be installed on the target server

## OS
* Ubuntu 14
* Ubuntu 16
* Ubuntu 18

## Role Varialbes
```bash

# defaults
package_name:
  - mongodb-org=3.4.10
  - mongodb-org-server=3.4.10
  - mongodb-org-shell=3.4.10
  - mongodb-org-mongos=3.4.10
  - mongodb-org-tools=3.4.10

version: "3.4"
key_server: "keyserver.ubuntu.com"
key_id: "0C49F3730359A14518585931BC711F9BA15703C6"
mongo_storage_path: "/var/lib/mongodb"
mongo_log_path: "/var/log/mongodb/mongod.log"

# vars
mongo_port_num: "27017"
replication_enabled: "yes"
replica_set_name: "rs1"
mongo_replica_group: mongo_replica
mongo_master: mongo1
```

## hosts file
```
mongo1 ansible_user=ubuntu ansible_host=10.0.6.24 ansible_private_key_file=/home/ubuntu/accesskey.pem
mongo2 ansible_user=ubuntu ansible_host=10.0.6.94 ansible_private_key_file=/home/ubuntu/accesskey.pem
mongo3 ansible_user=ubuntu ansible_host=10.0.6.44 ansible_private_key_file=/home/ubuntu/accesskey.pem

[mongo_replica]
mongo1
mongo2
mongo3
```

## Mongo Without Replication
If you do not want replication of mongo, you have provide "no" to "replication_enabled" in main.yml from vars directory, and difine your remote server in hosts file

## Mongo With Replication
Replication is the process of synchronizing data across multiple servers. Replication provides redundancy and increases data availability with multiple copies of data on different database servers. Replication protects a database from the loss of a single server. Replication also allows you to recover from hardware failure and service interruptions. Following are the steps you should follow to create replication of mongo
* Firstly define information of remote servers, for instance, private ip address, username etc. in hosts file
* Provide "yes" value to "replication_enabled" variable in main.yml from vars directory
* Provide name of group from hosts file in "mongo_replica_group" variable, mongo will use these hosts define under mongo_replica_group to create replication of mongo.
* Provide name of your master mongo server to "mongo_master" variable, This server will be the primary server, and other servers going to be secondary servers or failover servers.

#### Note
```
If you are going to create a replication of mongo with this role, your all servers must be in same and private network, and provide private ips of remote server also
```

```shell
mongod
├── defaults
│   └── main.yml
├── handlers
│   └── main.yml
├── README.md
├── hosts
├── tasks
│   ├── main.yml
│   ├── replica_ubuntu.yml
|   ├── ubuntu14.yml
│   └── ubuntu16.yml
├── templates
|   ├── mongodb.conf.j2
|   └── initial_replica.js.j2
└── vars
    └── main.yml
```


## Example Playbook
Here is an examples for the main playbook

Playbook to run mongod role without repliation
```yaml
---
- hosts: "{{ host_name }}"
  become: true
  roles:
    - mongod
...
```

Playbook to run mongod role with replication
```yaml
---
- hosts: mongo_replica
  become: true
  roles:
    - mongod
...
```
## Playbook Execution

#### Mongo Without Replication
Execute the playbook using the below command with required extra variables:(for example: "host_name=apache")

`ansible-playbook site.yml -e -i hosts "host_name=HOST"`

#### Mongo With Replication
`ansible-playbook -i hosts site.yml`
