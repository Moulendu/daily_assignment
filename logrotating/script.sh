#!/bin/bash
mv /apps/trucking/logs/*.gz /apps/trucking/logs/archive1
aws s3 cp --region ap-south-1 /apps/trucking/logs/archive1/ s3://wheelseye-logs/logs/ --recursive --exclude "*" --include "*.gz"

mv /apps/trucking/logs/archive1/*.gz /apps/trucking/logs/archive2
find /apps/trucking/logs/archive2 -type f -name '*.gz' -mtime +3 -exec rm {} \;
~            
