#!/bin/sh
check_tomcat() {
	count=$(sudo find /opt -name "apache-tomcat-*" | wc -l)
	if [ ${count} -gt 0 ]
	then
		echo "tomcat already available"
		exit
	fi
	}
check_java() {
	count=$(find $JAVA_HOME -name bin | wc -l)
	if [ ${count} -gt 0 ]
	then
		echo "java is set"
	else
		echo "your java is not set"
		exit
	fi
	

}
install_in_ubuntu() {
	echo "installing"
	tomcat_full_version=${1}
	tomcat_short_version=${2}
	wget mirrors.estointernet.in/apache/tomcat/tomcat-${tomcat_short_version}/v${tomcat_full_version}/bin/apache-tomcat-${tomcat_full_version}.tar.gz
sudo tar xzvf apache-tomcat-*tar.gz -C ~/tomcat 
}
install_in_centos() {
        echo "installing"
        tomcat_full_version=${1}
        tomcat_short_version=${2}
	echo $tomcat_short_version
          wget mirrors.estointernet.in/apache/tomcat/tomcat-${tomcat_short_version}/v${tomcat_full_version}/bin/apache-tomcat-${tomcat_full_version}.tar.gz
sudo tar xzvf apache-tomcat-*tar.gz -C ~/tomcat

}

check_tomcat
check_java
#echo "contineu"
tomcat_full_version=${1}
tomcat_short_version=$(echo ${1} | cut -d'.' -f1)
echo $tomcat_short_version
os_type=$(sudo cat /etc/os-release | awk -F'"' 'NR==5{print $2}' | cut -d' ' -f1)
if [ ${os_type} = "Ubuntu" ]
then
	
	install_in_ubuntu ${tomcat_full_version} ${tomcat_short_version}
	
elif [ ${os_type} = "centos" ]
then
	install_in_centos ${tomcat_full_version} ${tomcat_short_version}
	
fi
