provider "aws" {
  region = "us-east-1"
}

module "my_vpc" {
  source      = "../modules/vpc"
  vpc_cidr    = "10.100.0.0/16"
  vpc_name    = "mg1-vpc"
  vpc_id      = "${module.my_vpc.vpc_id}"
  subnet_cidr = "10.100.1.0/24"
  subnet_name = "mg1-subnet"

}
module "my_ec2" {
  source      = "../modules/ec2"
  ami_id      = "ami-0a313d6098716f372"
  instance_type = "t2.micro"
  subnet_id   = "${module.my_vpc.subnet_id}"
}
