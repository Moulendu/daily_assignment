#!/bin/bash
function gitAddCommit() { 
	file_name="${1}"
	commit_message="${2}"
	git add "${file_name}"
	git commit -m "${commit_message}" | awk 'NR==1{print $0}'

}
function QAreview() {
	review_message="${1}"
	echo "QA: ${review_message}"

}
function UATreview() {
        review_message="${1}"
        echo "BUSINESS: ${review_message}"

}
function CUSTOMERreview() {
        review_message="${1}"
        echo "CUSTOMER: ${review_message}"

}


function updateCommit() {
	commit_message="${1}"
	git commit -m "${commit_message}" | awk 'NR==1{print $0}'

}

function createFile() {
	file_name="${1}"
	echo "creating file ${file_name} to add an employee to database"
	echo "enter content of ${file_name}"
	read content
	echo ${content} > "${file_name}"

}
function updateFile() {
        file_name="${1}"
        echo "update content of ${file_name}"
        read content
        echo ${content} >> "${file_name}"
	git add ${file_name}
}
function showLogOf() {
	week="${1}"
	echo -e "\n ${1} LOG DETAILS \n"
	git log --pretty=format:"%cd,%an,%ae,%B" | sed '1i\DATE-TIME,AUTHOR_NAME,AUTHOR_EMAIL,COMMIT_MESSAGE' | column -t -s','
}

echo -e " \n ------------------- WEEK1---------------- \n"
touch README.md
gitAddCommit README.md "initial commit"

git checkout -b "release1"

createFile EmployeeDAO.java
gitAddCommit EmployeeDAO.java "JIRA-001:EmployeeDAO is created"
createFile EmployeePOJO.java
gitAddCommit EmployeePOJO.java "JIRA-002:EmployeePOJO is created"
createFile EmployeeController.java
gitAddCommit EmployeeController.java "JIRA-003:EmployeeController is created"
createFile AddEmployee.jsp
gitAddCommit AddEmployee.jsp "JIRA-004:AddEmployee is created"

git checkout master

git merge release1 >> merge.log
showLogOf WEEK1

echo -e " \n ------------WEEK2------------ \n"

git checkout release1

QAreview "JIRA-101:RELEASE1:BUG FOUNDED-CREATE A VALIDATION FOR EMAIL ID " 
updateFile AddEmployee.jsp
updateFile EmployeeDAO.java
updateCommit "JIRA-101:A VALIDATION FOR EMAIL ID IS ADDED"
QAreview "NO BUGS FOUNDED"

git checkout -b "release2"

updateFile EmployeeDAO.java
updateCommit "JIRA-001:UPDATED TO LIST EMPLOYEE"
updateFile EmployeeController.java
updateCommit "JIRA-003:UPDATED"
createFile ListEmployee.jsp
gitAddCommit ListEmployee.jsp "JIRA-001:RELEASE2:TO LIST EMPLOYEES"

git checkout master

git merge -s ours release1 >> merge.log
git merge -s ours release2 >> merge.log


showLogOf WEEK2

echo -e " \n -------------WEEK3-------------- \n"

git checkout release1

UATreview "JIRA-102:ISSUE FOUNDED-USER INPUT SCREEM IS NOT ACCORDING TO THE BUSINESS REQUIREMENT"
updateFile AddEmployee.jsp
git add AddEmployee.jsp
updateCommit "JIRA-102:USER SCREEN ISSUE IS FIXED"
QAreview "THERE IS NO BUGS, RELEASE IS READY FOR UAT"
UATreview "NO ISSUE FOUNDED,REALEASE IS READY FOR CUSTOMER"

git checkout release2

QAreview "JIRA-101:REALEASE2:NAMES ARE NOT GETTING SORTED"
updateFile EmployeeController.java
updateCommit "JIRA-101:RELEASE2:SORTING PROBLEM IS SORTED"
QAreview "NO BUGS"

git checkout -b "release3"

updateFile EmployeeDAO.java
updateCommit "JIRA-001:RELEASE3:UPDATED"
updateFile EmployeeController.java
updateCommit "JIRA-003:RELEASE3:UPDATED TO UPDATE USER DETAILS"
createFile EditEmployee.jsp
gitAddCommit EditEmployee.jsp "JIRA-006:REALEASE3:TO UPDATE EMPLOYEE DATA"

git checkout master

git merge -s ours release1 >> merge.log
git merge -s ours release2 >> merge.log
git merge -s ours release3 >> merge.log


showLogOf WEEK3


echo -e " \n ----------WEEK4---------- \n"

git checkout release1

CUSTOMERreview "JIRA-103:ISSUE FOUNDED-WRONG PHONE NUMBER"
UATreview "ADD OTP TO VALIDATE PHONE NUMBER"
updateFile  EmployeeController.java
git add EmployeeController.java
updateCommit "JIRA-103:OTP FUNCTIONALITY IS ADDED"
QAreview "THERE IS NO BUGS, RELEASE IS READY FOR UAT"
UATreview "NO ISSUE FOUNDED,REALEASE IS READY FOR CUSTOMER"

git checkout release2

UATreview "JIRA-101:RELEASE2:EMPLOYEE LISTING PAGINATION IS NOT WORKING"
updateFile ListEmployee.jsp
updateCommit "JIRA-101:RELEASE2:EMPLOYEE LISTING PAGINATION PROBLEM SOLVED"
QAreview "NO BUGS,READY FOR UAT"
AUTreview "NO ISSUE,READY FOR CUSTOMER"

git checkout release3

QAreview "JIRA-101:RELEASE3:EMPLOYEE LAST NAME IS NOT GETTING UPDATE"
updateFile EmployeeController
updateCommit "JIRA-101:RELEASE3:NAME PROBLEM SOLVED"
QAreview "NO BUG,READY FOR UAT"

git checkout -b "release4"

updateFile EmployeeDAO.java
updateCommit "JIRA-001:RELEASE4:UPDATED FOR EMPLOYEE DELETION"
updateFile ListEmployee.jsp
updateCommit "JIRA-002:RELEASE4:UPDATED DELETE FUNCTIONALITY"

git checkout master

git merge -s ours release1 >> merge.log
git merge -s ours release2 >> merge.log
git merge -s ours release3 >> merge.log
git merge -s ours release4 >> merge.log

showLogOf WEEK4

