#!/bin/bash
version=$( sudo cat /etc/os-release | awk -F'"' 'NR==5{print $2}' | cut -d' ' -f1)
pack=${1}
if [ ${version} = "Ubuntu" ]
then
	n=$(sudo apt list --installed ${pack} | wc -l)
	if [ ${n} -gt 1 ]
	then
		m=$(sudo apt list --upgradable $1 | wc -l)
		if [ ${m} -gt 1 ]
		then
			echo "${pack} is already installed and upgrade is availabe"
		else
			echo "${pack} is installed no upgrade is available"
		fi

	else
	sudo apt-get install ${pack}
	fi
elif [ ${version} = "centos" ]
then
	n=$(sudo yum list installed ${pack} | wc -l)
        if [ ${n} -gt 1 ]
        then
                m=$(sudo yum list updates ${pack} | grep $1 | wc -l)
                if [ ${m} -gt 1 ]
                then
                        echo "${pack} is already installed and upgrade is availabe"
                else
                        echo "${pack} is installed no upgrade is available"
                fi

        else
		sudo yum install ${pack}
	fi

else
	echo "your os version is not supported"
fi
