#!/bin/bash
host_ip=$( ifconfig | grep 'inet addr' | grep '1[678][0-9]*' | cut -d : -f2 | cut -d ' ' -f 1 )
mv /apps/trucking/logs/*.gz /apps/trucking/logs/archive1
aws s3 cp --region ap-south-1 /apps/trucking/logs/archive1/ s3://wheelseye-logs/logs/pre_prod/${host_ip} --recursive --exclude "*" --include "*.gz"

mv /apps/trucking/logs/archive1/*.gz /apps/trucking/logs/archive2
find /apps/trucking/logs/archive2 -type f -name '*.gz' -mtime -1 -exec rm {} \;

